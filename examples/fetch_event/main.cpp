# include "na64ee_remote_readout.hpp"  // for event ID routines
# include <cstring>
# include <DaqEvent.h>

int
main() {
    // One has to specify the data server WWW-address or IP address here:
    // Variants:
    //const char * datServerAddress = "http://127.0.0.1";
    //const char * datServerAddress = "http://pcdmfs01";
    const char datServerAddress[] = "http://127.0.0.1:5000/";

    // Get the event now:
    std::shared_ptr<CS::DaqEvent> evPtr = na64ee::fetch_event(
            datServerAddress,
            # if 0 //
            /* Run number ................... */ 2545,
            /* Spill number in this run ..... */ 6,
            /* Event number in this spill ... */ 2321 );
            # else
            /* Run number ................... */ 1436,
            /* Spill number in this run ..... */ 154,
            /* Event number in this spill ... */ 1234 );
            # endif
    std::cout << " run#" << evPtr->GetRunNumber() << std::endl
              << " burst#" << evPtr->GetBurstNumber() << std::endl
              << " event#" << evPtr->GetEventNumberInBurst() << std::endl
              ;

    // ... do operations with an event object referred by eventObj.

    return 0;
}

