# include "na64_event_id.hpp"  // for event ID routines
# include <DaqEvent.h>  // for CS::DaqEvent type

using namespace na64;
using namespace CS;

int
main() {

    EventIDs eids;
    eids.push_back( 1137, 56, 25567 );
    eids.push_back( 1247, 112, 1837 );
    eids.push_back( 1247, 112, 1838 );
    // ...

    // Get the event now:
    auto & events = fetch_events( "http://na64-data-server.example.org", eids );

    for( int i = 0; i < events.size(); i++ ) {
        DaqEvent & event = events[i];
        // ... do operations with an event object referred by eventObj.
    }

    return 0;
}



