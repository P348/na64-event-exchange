# include "na64_event_id.hpp"

using namespace na64;

int
main() {
    // To create numeric identifier of an event one need number of the run,
    // number of the spill in this run, and number of an event in spill:
    int runNo   = 1247;
    int spillNo = 112;
    int eventNo = 876;

    // One can now pack all the numbers into single numeric ID:
    AbsEventID evID = na64::assemble_event_id( runNo, spillNo, eventNo );

    // ... one can further operate with this evID variable as it is an ordinary
    // number.

    return 0;
}

