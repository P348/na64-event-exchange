# Event exchange library for NA64

This library was crafted to provide the additional utility interfaces for
exchanging raw statistics yielded by NA64 experiment (CERN, SPS) data
acquizition system.

The format of the data itself is identical to one used in NA58 (a.k.a COMPASS,
CERN, SPS). The basic interfaces for accumulation, (en/de)coding, retrieval and
basic managing the data is provided by DaqDataDecoding library included in CORAL
fork in the [p348-daq](https://gitlab.cern.ch/P348/p348-daq/) distribution.

The p348-daq library also ships "official" version of somewhat called
"reconstruction library" (that is merely a C++ header file in the master
branches).

The NA64EE library was designed to provide some advanced functions for raw data
management as well as nicer adaptation for CMake build system. It's primary
goal was to immerse the raw statistics management routines into wider complex
frameworks by mean of modern software (CMake, python bindings). The library
with supplementary applications and scripts may be used as a standalone tool
or being embedded into [NA64-meta](https://gitlab.cern.ch/P348/NA64-meta) git
superproject.


## What is inside this package?

The NA64EE library and few utility applications:
- `batch_readout` --- a simple application providing the the particular events
reading (picking off). Accepts the `.dat` file and offsets cache file as
cmd-line arguments and expects numerical event IDs on its input. This directory
also contains the `batch-events-read.py` script that one may find convinient
for reading vast amount of events from hundreds of chunks using interim caches.
- `dump_evnums` --- a very simple demo application performing walk-through
events reading from the .dat file, dumping their IDs.
- `na64ee-mdat`/`na64-read-one` (`mdatee` dir) --- the former one performs
extraction of an offsets cache into file. The second one may utilize this
caches to perform single event extraction then.
- `na64-read-test` --- a demonstration application performing various tests
of NA64EE library on the given .raw file.

All these application is included into major library's `CMakeLists.txt` file
via the `add_subdirectory()` command and will be automatically built after the
library itself.

The library will also generate few `.cmake` configuration files allowing
further reentrant usage as an imported target or via the `find_package()`
command.


## Requirements

It is implied that from the [p348-daq](https://gitlab.cern.ch/P348/p348-daq/)
one has to build and install from branch
[p348reco-so](https://gitlab.cern.ch/P348/p348-daq/tree/p348reco-so/) (since
the master branch does not support building shared library yet):
- The DaqDataDecoding library 
- p348reco library

The subsequent dependencies (ones, from which both these libraries are
dependant) aren't considered here.

