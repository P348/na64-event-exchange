/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_EVENT_EXCHANGE_ERROR_HANDLE_H
# define H_NA64_EVENT_EXCHANGE_ERROR_HANDLE_H

/**@brief An X-macro for NA64 error codes listing their numerical code, name
 * and detailed string description for using at the verbose messages.
 * */
# define for_all_NA64EE_error_codes(m) \
    m( 0x1,     notImplemented, "Requested routine is not yet implemented." )   \
    m( 0x2,     overflow,       "Overflow occured in event numerical ID." )     \
    m( 0x3,     underflow,      "Underflow occured in event numerical ID." )    \
    m( 0x4,     notInSpill,     "Event not found in a specific spill (burst)." )\
    m( 0x5,     notInChunk,     "Event not found in a specific chunk (file).")  \
    m( 0x6,     notInRun,       "Has no such event in a specific run." )        \
    m( 0x7,     mergeMismatch,  "Merging revealed a disagreement." )            \
    m( 0x8,     badRunNumber,   "Wrong run number suggested." )                 \
    m( 0x9,     eventNotFound,  "Event with specific ID not found." )           \
    m( 0xa,     fileNotFound,   "File not found at specific location." )        \
    /* ... */

# ifdef __cplusplus
extern "C" {
    # define NA64EE_EnumScope_PREFIX NA64EE_EnumScope::
# else
    # define NA64EE_EnumScope_PREFIX enum
# endif

typedef struct NA64EE_EnumScope {
    enum NA64EE_ErrorCode {
        # define declare_cst(code, name, description) na64ee_ ## name = code,
        for_all_NA64EE_error_codes(declare_cst)
        # undef declare_cst
    } _dummy;
} NA64EE_EnumScope;  // struct NA64EE_EnumScope

/**@brief Returns pointer to null-terminated C-string containing the
 * description of NA64EE-lib error's numerical code.
 * */
const char * na64ee_error_code_description( NA64EE_EnumScope_PREFIX NA64EE_ErrorCode );

/**@brief An error handler public function pointer.
 *
 * One can customize actions which take place upon NA64EE lib raises an error
 * by overriding this pointer. It is invoked by lib when any of foreseen errors
 * arised (meaning, the foreseen assertion usually blaming third-party code).
 *
 * The function will receive the error code (one of the defined in enumeration
 * NA64EE_EnumScope::ErrorCode and null-terminated C-string describing an
 * error's details).
 *
 * For attributes of function pointers, see:
 *  - https://gcc.gnu.org/bugzilla/show_bug.cgi?id=3481
 *  - http://stackoverflow.com/questions/9441262/function-pointer-to-attribute-const-function
 * */
extern void (* NA64EE_C_errorHandle)(
        NA64EE_EnumScope_PREFIX NA64EE_ErrorCode,
        const char * ) __attribute__ ((noreturn));

/**@brief A default error handling function of NA64EE library.
 *
 * Prints out the error code, it's details and immediately aborts execution.
 * */
void NA64EE_C_default_error_handle(
        NA64EE_EnumScope_PREFIX NA64EE_ErrorCode,
        const char * ) __attribute__ ((noreturn));

/**@brief Invokes an error handler for NA64EE lib.
 *
 * Printf-like function getting the error enum code, format string, and
 * (optionally) a set of  format arguments. Has no-return GCC-specific
 * attribute (also supported by CLANG) meaning that, upon entry, execution
 * can not be returned. That is standard feature for all error-handling
 * functions allowing the compiler perform a better optimization and smoothly
 * resolve some compile-time sematics.
 * */
void na64ee_C_error( NA64EE_EnumScope_PREFIX NA64EE_ErrorCode,
                     const char * fmt, ... ) __attribute__ ((noreturn));

# ifdef __cplusplus
}  // extern "C"
# endif

# endif  /* H_NA64_EVENT_EXCHANGE_ERROR_HANDLE_H */

