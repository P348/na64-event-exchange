/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_EVENT_IDENTIFIERS_H
# define H_NA64_EVENT_IDENTIFIERS_H

# include "na64ee_config.h"

/**@file na64_event_id.h
 *
 * @brief This file declares structures and uxilliary routines for identifying
 *        and event in the NA64 middleware routines.
 *
 * Considering the need to accumulate <=1e14 events per NA64 experiment this
 * structures were carefully crafted in order to provide event identification
 * mechanizm minimizing computational cost.
 *
 * One can make a distinction between event number identifying certain event
 * instance inside all the run or inside particular chunk in run. By technical
 * reasons it is desirable to identify events inside the chunk as it seems to
 * be cheaper to read the event from particular file (which is a chunk itself)
 * then to look up it inside all the files of a particular run.
 *
 * Here we provide both ways to identify event.
 * */

# include <limits.h>
# include <stdio.h>
# include <stdint.h>

# include "na64ee_config.h"

/** Plain numeric type identifying the certain event in NA64 experiment. */
typedef uint64_t NA64_EventNumericID;

/**@macro for_all_eventID_fields
 * @brief X-Macro indexing all the bit fields of the event ID.
 * */
# define for_all_chunkless_eventID_fields( m, ... )                                             \
    /** Number of the run */                                                                    \
    m( uint16_t,    runNo,      16, "#run"              RUN_NO_MAX,             __VA_ARGS__ )   \
    /** Indicates whether struct has a chunk number (reserved). */                              \
    m( uint8_t,     hasChunkNo,  1,  "has-chunk-no?",    1,                      __VA_ARGS__ )  \
    /** Number of the spill in chunk */                                                         \
    m( uint16_t,    spillNo,    16, "#spill-in-run",    SPILL_IN_RUN_NO_MAX,    __VA_ARGS__ )   \
    /** Number of the event in spill */                                                         \
    m( uint32_t,    eventNo,    31, "#event-in-spill",  EVENTINSPILL_NO_MAX,    __VA_ARGS__ )

# define declare_type( bfType, bfName, bfSize, bfDescr, bfMax, ... ) \
        typedef bfType NA64_ ## bfName ## _t;
for_all_chunkless_eventID_fields( declare_type )
# undef declare_type

/** Upper limit of run number */
# define RUN_NO_MAX             ((unsigned long)USHRT_MAX)
/** Upper limit of spill-in-chunk number */
# define SPILL_IN_RUN_NO_MAX    ((unsigned long)(((1UL) << 16 )-1))
/** Upper limit of event-in-spill number */
# define EVENTINSPILL_NO_MAX    ((unsigned long)(((1UL) << 31)-1))

/**@struct NA64_EventID
 * @brief The event's ID packed structure without chunk info.
 *
 * This struct keeps the Unique ID of an event. It has a fixed, restricted
 * length and can be converted to numeric EventNumericID type without any
 * overhead.
 *
 * Basically, it consists of the following set of values:
 *      runNo/chunkNo/spillNo/eventInSpillNo
 * where
 *  - runNo             the number of the run
 *  - spillNo           the number of spill inside the run
 *  - eventInSpillNo    the number of event inside the spill
 *
 * Additionally, one bit is reserved for this structure to indicate that type
 * has a chunk number (hasChunkNo has to be set to 0).
 */
typedef struct __attribute__((__packed__)) NA64_EventID {
    # define declare_bitfield( bfType, bfName, bfSize, bfDescr, bfMax, ... ) \
        bfType bfName : bfSize;
    for_all_chunkless_eventID_fields( declare_bitfield )
    # undef declare_bitfield
} NA64_EventID;



/**@macro for_all_eventID_fields
 * @brief X-Macro indexing all the bit fields of the event ID.
 * */
# define for_all_eventID_fields( m, ... )                                                       \
    /** Number of the run */                                                                    \
    m( uint16_t,    runNo,      16, "#run"              RUN_NO_MAX,             __VA_ARGS__ )   \
    /** Indicates whether struct has a chunk number (reserved). */                              \
    m( uint8_t,     hasChunkNo,  1,  "has-chunk-no?",    1,                      __VA_ARGS__ )  \
    /** Number of the chunk in run */                                                           \
    m( uint8_t,     chunkNo,    7,  "#chunk-in-run",    CHUNK_NO_MAX,           __VA_ARGS__ )   \
    /** Number of the spill in chunk */                                                         \
    m( uint16_t,    spillNo,    9,  "#spill-in-chunk",  SPILL_IN_CHUNK_NO_MAX,  __VA_ARGS__ )   \
    /** Number of the event in spill */                                                         \
    m( uint32_t,    eventNo,    31, "#event-in-spill",  EVENTINSPILL_NO_MAX,    __VA_ARGS__ )

/** Upper limit of chunk number */
# define CHUNK_NO_MAX           ((unsigned long)(((1UL) << 7 )-1))
/** Upper limit of spill-in-chunk number */
# define SPILL_IN_CHUNK_NO_MAX  ((unsigned long)(((1UL) << 9 )-1))

/**@struct NA64_StoredEventID
 * @brief The event's ID packed structure with chunk info (reliable, preferred
 *        form).
 *
 * This struct keeps the Unique ID of an event. It has a fixed, restricted
 * length and can be converted to numeric EventNumericID type without any
 * overhead.
 *
 * Basically, it consists of the following set of values:
 *      runNo/chunkNo/spillNo/eventInSpillNo
 * where
 *  - runNo             the number of the run
 *  - chunkNo           the number of the chunk (file) in run
 *  - spillNo           the number of spill inside the chunk
 *  - eventInSpillNo    the number of event inside the spill
 *
 * Additionally, one bit is reserved for this structure to indicate that type
 * has a chunk number (hasChunkNo has to be set to 1).
 */
typedef struct __attribute__((__packed__)) NA64_StoredEventID {
    # define declare_bitfield( bfType, bfName, bfSize, bfDescr, bfMax, ... ) \
        bfType bfName : bfSize;
    for_all_eventID_fields(declare_bitfield)
    # undef declare_bitfield
} NA64_StoredEventID;

/**@union UEventID
 * @brief An uxilliary union keeping all the event IDs types
 *        altogether.
 *
 * Use this union for fast conversion between different types of event
 * identifiers.
 */
typedef union NA64_UEventID {
    NA64_EventID chunklessLayout;
    NA64_StoredEventID layout;
    NA64_EventNumericID numeric;
} NA64_UEventID;

# define EVENTID_STR_BUFLENGTH (2*sizeof(NA64_UEventID) + 1)

/*
 * Routines
 * ////// */

# ifdef __cplusplus
extern "C" {
# endif  /*__cplusplus*/

/** Print basic type info to output stream referred by C FILE handle (one can
 * provide stdout as well) */
void
NA64EE_print_eventID_typeinfo( FILE * );

/** Performs testing operations to valiadate event IDs type structures. To be
 * further invoked by unit testing facility. */
int
NA64EE_test_eventID_typedecls( FILE * errorStreamHandle );

/** Common ID constructor with available chunk number. */
NA64_EventNumericID
NA64EE_assemble_stored_event_id(
        NA64_runNo_t    runNo,
        unsigned char   chunkNo,
        unsigned short  spillNo,
        unsigned int    eventInSpillNo );

/** Commoin ID constructor without available chunk number. */
NA64_EventNumericID
NA64EE_assemble_event_id(
        NA64_runNo_t    runNo,
        unsigned short  spillNo,
        unsigned int    eventInSpillNo );

/** Use this function to register printing conversion for functions in
 * printf()-family. */
void NA64EE_register_event_id_print( const char );

/** Encodes event ID into given string buffer */
size_t eid2str(NA64_UEventID eid, char * buf, size_t buflen);

/** Encodes eid to hex string writing the string into provided buffer
 * that has to be of length 2*sizeof(NA64_EventID) + 1.
 * */
void eid2hex_str( NA64_UEventID eid, char * buffer );

int
hex_str2eid(    const char * buffer,
                NA64_UEventID * resultPtr );


# ifdef __cplusplus
}
# endif  /*__cplusplus*/

# endif  /* H_NA64_EVENT_IDENTIFIERS_H */




