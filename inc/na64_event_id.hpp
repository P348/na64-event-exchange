/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_EVENT_ID_METADATA_DICTIONARY_H
# define H_NA64_EVENT_ID_METADATA_DICTIONARY_H

# include "na64_event_id.h"

# include <iostream>
# include <set>
# include <utility>

namespace na64ee {

typedef ::NA64_EventNumericID EventNumericID;
typedef ::NA64_UEventID UEventID;
typedef ::NA64_StoredEventID StoredEventID;
typedef ::NA64_EventID AbsEventID;

std::ostream & operator<<( std::ostream & stream, const UEventID & eid );

# define for_all_NA64EE_base_eid_routines( m )  \
    m( print_eventID_typeinfo )                 \
    m( test_eventID_typedecls )                 \
    m( assemble_stored_event_id )               \
    m( assemble_event_id )                      \
    m( register_event_id_print )                \
    /* ... */

# define declare_type( bfType, bfName, bfSize, bfDescr, bfMax, ... ) \
        typedef ::NA64_ ## bfName ## _t bfName ## _t;
for_all_chunkless_eventID_fields( declare_type )
# undef declare_type

# ifndef SWIG
# define declare_Cpp_alias( name ) extern decltype( NA64EE_ ## name ) & name;
for_all_NA64EE_base_eid_routines( declare_Cpp_alias )
# undef declare_Cpp_alias
# endif

/// Type describing a set of events. Sorted by design.
typedef std::set<StoredEventID> EventIDs;

/// Type describing a range of events (lower, upper).
typedef std::pair<StoredEventID, StoredEventID> EventsRange;

inline bool operator<(const AbsEventID & a, const AbsEventID & b) {
    return (UEventID{.chunklessLayout = a}).numeric < (UEventID{.chunklessLayout = b}).numeric;
}


struct EventID {
private:
    UEventID _ueid;
public:
    EventID( NA64_runNo_t runNo
           , NA64_spillNo_t spillNo
           , NA64_eventNo_t evNo ) : _ueid{ .chunklessLayout{runNo, 0, spillNo, evNo} } {}

    operator EventNumericID () const { return _ueid.numeric; }

    NA64_runNo_t run_no() const {
        return _ueid.chunklessLayout.runNo;
    }

    void run_no( runNo_t rn ) {
        _ueid.chunklessLayout.runNo = rn;
    }

    auto chunk_no() const {
        if( ! _ueid.layout.hasChunkNo ) {
            throw std::runtime_error( "Event ID bears no chunk number." );
        }
        return _ueid.layout.chunkNo;
    }

    // TODO
    void chunk_no( uint32_t cn ) {
        if( ! _ueid.layout.hasChunkNo ) {
            throw std::runtime_error( "Event ID bears no chunk number." );
        }
        _ueid.layout.chunkNo = cn;
    }
};

}  // namespace na64

# endif  // H_NA64_EVENT_ID_METADATA_DICTIONARY_H

