/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_EVENT_EXCHANGE_CONFIG_H
# define H_NA64_EVENT_EXCHANGE_CONFIG_H

/* NOTE: AUTOMATICALLY GENERATED CONTENT
 * this file exists in repository in two forms: with and withoun .in postfix.
 * Please, be aware of editing the file without .in name postfix since all
 * changes will be lost after next invokation of CMake routines.
 */

#cmakedefine NA64EE_VERSION_MAJOR @NA64EE_VERSION_MAJOR@
#cmakedefine NA64EE_VERSION_MINOR @NA64EE_VERSION_MINOR@
#cmakedefine NA64EE_VERSION_PATCH @NA64EE_VERSION_PATCH@
#cmakedefine NA64EE_VERSION       @NA64EE_VERSION@
#cmakedefine SUPPORT_DDD_FORMAT   @SUPPORT_DDD_FORMAT@
#cmakedefine CURL_FOUND           @CURL_FOUND@
#cmakedefine UT_ENABLED           @UT_ENABLED@

/**@brief event ID identifier for printf()-like functions.
 *
 * It is advisable not to use lowercase letters, since the ISO C standard warns
 * that additional lowercase letters may be standardized in future editions of
 * the standard.
 *
 * Since we can not provide custom format character for GCC/CLANG format check,
 * utilizing of this character is not recommended as it causes compiler to
 * produce -Wformat warnings wherever it will see it. See:
 *      https://gcc.gnu.org/bugzilla/show_bug.cgi?id=47781
 * */
# define EVENT_PRINTF_FORMAT_CHAR V
# define fmt_EVENTID STRINGIFY_MACRO_ARG(EVENT_PRINTF_FORMAT_CHAR)

# ifndef STRINGIFY_MACRO_ARG
#   define STRINGIFY_MACRO_ARG(a) _STRINGIFY_MACRO_ARG(a)
#   define _STRINGIFY_MACRO_ARG(a) #a
# endif

# define DEFAULT_RESOURCES_URL "http://127.0.0.1:5000/"
# define SINGLE_EVENT_PREFIX "rawstat/data/event/"

# if __cplusplus <= 199711L
#   define nullptr_C11 NULL
# else
#   define nullptr_C11 nullptr
# endif

# endif  /* H_NA64_EVENT_EXCHANGE_CONFIG_H */

