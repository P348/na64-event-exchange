/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_EVENT_EXCHANGE_DDD_READOUT_H
# define H_NA64_EVENT_EXCHANGE_DDD_READOUT_H

# include "na64_event_id.hpp"

# ifdef SUPPORT_DDD_FORMAT

# include <iosfwd>
# include <map>
# include <list>
# include <memory>
# include <vector>

namespace CS {
class DaqEvent;  // FWD
}  // namespace CS

namespace na64ee {

/// This function causes setting the position of input stream to be set at
/// particular event ID number.
bool
direct_look_up_event( std::istream &, const AbsEventID & );

/**@brief container structure representing data layout inside one spill.
 *
 * This auxilliary struct keeps interim data describing the events layout
 * inside spill written in a file (typically, chunk).
 * */
struct SpillDataLayout_t {
    typedef uint8_t  ChunkNo_t;
    typedef uint16_t SpillNo_t;
    typedef uint32_t EventNo_t;
    typedef std::basic_istream<char>::pos_type FilePostion_t;

    /// Field referring to particular chunk number.
    ChunkNo_t chunkNo;
    /// Ordered map of event positions info in form <#event, #bytesOffset>.
    /// Can be sparsed.
    std::map<EventNo_t, FilePostion_t> eventPositions;
    /// Range of events no in ch
    EventNo_t eventsInSpill;

    /// Merges with another instance. Returns number of unique entries.
    size_t merge( const SpillDataLayout_t & );

    /// Merges with another instance. Returns number of unique entries.
    size_t merge( const std::map<EventNo_t, FilePostion_t> & );

    /// Returns pair of event number and its offset for the nearest
    /// preceding event. Can raise NotInSpill exception.
    std::pair<EventNo_t, FilePostion_t> get_preceding_event_offset(
                                                        EventNo_t ) const;
};

/**@brief An index class describing DDD file's metadata.
 *
 * This class imposed at basic abstraction layer and probably has never be
 * used by user code unless the cases when structure other than physical
 * chunks has to be utilized. The chunks with their aux info are considered
 * in descendant class.
 *
 * Instances of this class usually corresponds to physical artifacts (files).
 * It caches positional info for events. This information is structured in
 * sorted associative arrays. Despite the unsorted containers will be faster
 * the sorted ones is more readable.
 * */
class DDDIndex {
public:
    typedef SpillDataLayout_t SpillDataLayout;
    typedef SpillDataLayout::ChunkNo_t ChunkNo_t;
    typedef SpillDataLayout::SpillNo_t SpillNo_t;
    typedef SpillDataLayout::EventNo_t EventNo_t;
    typedef SpillDataLayout::FilePostion_t FilePostion_t;

    typedef uint16_t RunNo_t;
    /// Parent structure for run.
    typedef std::map<SpillNo_t, SpillDataLayout> RunDataLayout;
private:
    std::map<RunNo_t, RunDataLayout> _runs;
public:
    DDDIndex();
    DDDIndex( RunNo_t rn, const RunDataLayout & );
    DDDIndex( const DDDIndex & );

    virtual ~DDDIndex() {}

    /// Merges info from given index instance into current.
    size_t merge( const DDDIndex & );

    /// Merges obtained list of spill layout information into given dictionary.
    size_t merge( RunNo_t rn, const RunDataLayout & );

    /// Writes itself to bytestream.
    void write( std::ostream & ) const;

    /// Prints content in human-readable form. Can not be parsed.
    virtual void print_to_ostream( std::ostream & ) const;

    /// Prints content to JSON data.
    virtual void to_JSON_str( std::ostream &/*, bool prettyPrint=true*/ ) const;

    // Adds information from JSON string. (TODO)
    //virtual void from_JSON_str( std::istream & );

    /// Returns position in the stream corresponding to event specified by ID.
    FilePostion_t get_event_stream_pos( std::istream &,
                                      EventNumericID,
                                      FILE * dbgStream=nullptr_C11 ) const;

    /// Calculates the size of the buffer needed for serialization.
    size_t serialized_size() const;

    /// Serializes the object returning actual used bytes length.
    size_t serialize( uint8_t * bytes, size_t bytesLength ) const;


    /// Forms spill layout information for given stream.
    static void index_chunk( std::istream & is,
                             RunDataLayout & destLst,
                             size_t nDuty,
                             RunNo_t expectedRunNo,
                             ChunkNo_t defaultChunkNo,
                             FILE * dbgStream=nullptr_C11 );

    /// Deserialization class method returning pointer to dynamically allocated
    /// instance.
    static DDDIndex * deserialize( const uint8_t * bytes );
};  // class DDDIndex


/**@struct ChunkID
 * @brief Struct uniquely identifying raw data file.
 *
 * This union is used to identify data files (called "chunks") in NA64
 * experimental data.
 * */
struct ChunkID {
    DDDIndex::RunNo_t   runNo;
    DDDIndex::ChunkNo_t chunkNo;

    ChunkID( ) : runNo(0), chunkNo(0) {}
    ChunkID( DDDIndex::RunNo_t rn, DDDIndex::ChunkNo_t cn ) :
                                                    runNo(rn), chunkNo(cn) {}
};

std::ostream & operator<<(std::ostream & os, const ChunkID &);


/**@brief Utility function reading one raw event.
 *
 * Upon successfull reading, the newly-allocated event instance will be
 * returned.
 *
 * @exception fileNotFound when unable to reach the file
 * @exception eventNotFound when failed to look-up an event at the given
 *            position.
 * */
std::vector<uint8_t>
read_one_new_raw_event( const std::string & filePath,
                        const DDDIndex & idx,
                        NA64_UEventID );

}  // namespace na64ee

# else  // SUPPORT_DDD_FORMAT

# warning "This header declares routines and structs designed for dealing with " \
"DaqDataDecoding (DDD) library, however your build of NA64EE lib seems to be "   \
"not configured against DDD library and these routines are disabled."

# endif  // SUPPORT_DDD_FORMAT

# endif  // H_NA64_EVENT_EXCHANGE_DDD_READOUT_H


