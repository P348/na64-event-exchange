/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64ee_readout.hpp"

# include <cstdlib>
# include <fstream>

# include <DaqEvent.h>

int
main( int argc, char * const argv[] ) {
    {
        // Mandatory printf-initialization:
        const char fmt[] = fmt_EVENTID;
        NA64EE_register_event_id_print( fmt[0] );
    }

    // Ordinary C/C++ command line arguments treatment routines. First, check
    // that we have one argument provided within command line and print usage
    // info if not. Filename usually matches to regex:
    //      (([^\d]+)(\d+)-(\d+)(:?.dat))
    // Where, from two digits, the first corresponds to chunk number, and second
    // corresponds to run number.
    if( argc != 5 ) {
        fprintf( stderr,
                 "Usage:\n\t$ %s <in-filename.dat> <out-filename.mdat> "
                 "<spill-no> <event-in-spill-no>\n",
                 argv[0] );
        return EXIT_FAILURE;
    }

    char * fname = rindex( argv[1], '/' );
    if( !fname ) {
        fname = argv[1];
    } else {
        ++fname;
    }
    int runNo = -1,
        chunkNo = -1;
    sscanf( fname, "cdr%d-%d.dat", &chunkNo, &runNo );
    if( -1 == runNo || -1 == chunkNo ) {
        fprintf( stderr,
                 "Unable to interpret \"%s\" filename as chunk descriptor.\n"
                 "Expected: \"cdr-<chunkNo>-<runNo>.dat\".\n", fname);
        return EXIT_FAILURE;
    }
    int spillNo = atoi(argv[3]),
        eventNo = atoi(argv[4])
        ;

    // Try to open files:
    // - metadata:
    std::ifstream mdatIs( argv[2], std::ios::binary | std::ios::ate );
    if( !mdatIs ) {
        fprintf( stderr, "Unable to open file %s.\n", argv[2] );
        return EXIT_FAILURE;
    }
    std::streamsize size = mdatIs.tellg();
    mdatIs.seekg(0, std::ios::beg);
    std::vector<char> buffer(size);
    mdatIs.read(buffer.data(), size);
    na64ee::DDDIndex & idx =
                *na64ee::DDDIndex::deserialize( (uint8_t *) buffer.data() );
    mdatIs.close();
    // - statistics:
    std::ifstream is( argv[1] );
    if( !is ) {
        fprintf( stderr, "Unable to open file %s.\n", argv[1] );
        return EXIT_FAILURE;
    }

    // Read the specified event:
    // - assemble event number descriptor:
    na64ee::EventNumericID eid
                = na64ee::assemble_event_id( runNo, spillNo, eventNo );
    // - get the physical offset in file:
    na64ee::DDDIndex::FilePostion_t evPos
                              = idx.get_event_stream_pos( is, eid );
    is.seekg( evPos, is.beg );
    
    // One may construct a DDD's (undecoded) event now with:
    CS::DaqEvent daqEvent( is );

    // Check event numbers if need:
    if( daqEvent.GetRunNumber() != (unsigned) runNo
     || daqEvent.GetBurstNumber() != (unsigned) spillNo
     || daqEvent.GetEventNumberInBurst() != (unsigned) eventNo ) {
        fprintf( stderr, "Wrong event had been read.\n" );
        return EXIT_FAILURE;
    }

    // Event had been successfully read upon this line. One may perform
    // something meaningful here --- e.g. decode it with DaqEventsManager
    // or whatever.
    std::cout << "Event read." << std::endl;  // TODO

    is.close();

    return EXIT_SUCCESS;
}


