# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#
# Common CMake
cmake_minimum_required( VERSION 2.6 )
project( na64ee-mdatee-app )

set( NA64EE_MDAT_EXEC na64ee-mdat )
set( NA64EE_READ_EXEC na64ee-read-one )

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_RPATH}:${CMAKE_INSTALL_PREFIX}/lib/na64")

add_executable( ${NA64EE_MDAT_EXEC} main-mdatee.cpp )
add_executable( ${NA64EE_READ_EXEC} main-readee.cpp )
#target_link_libraries( ${NA64EE_MDAT_EXEC} ${Boost_LIBRARIES} )
if( NOT DAQDECODING_LIB_SHARED )
    target_link_libraries( ${NA64EE_MDAT_EXEC} ${DAQDECODING_LIB} )
    target_link_libraries( ${NA64EE_READ_EXEC} ${DAQDECODING_LIB} )
endif( NOT DAQDECODING_LIB_SHARED )
target_link_libraries( ${NA64EE_MDAT_EXEC} ${NA64EE_LIB} )
target_link_libraries( ${NA64EE_READ_EXEC} ${NA64EE_LIB} )

install( TARGETS ${NA64EE_MDAT_EXEC} ${NA64EE_READ_EXEC} RUNTIME DESTINATION bin )


