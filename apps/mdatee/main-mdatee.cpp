/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64ee_readout.hpp"

# include <cstdlib>
# include <fstream>

# include <DaqEvent.h>

int
main( int argc, char * const argv[] ) {
    {
        // Mandatory printf-initialization:
        const char fmt[] = fmt_EVENTID;
        NA64EE_register_event_id_print( fmt[0] );
    }

    // Ordinary C/C++ command line arguments treatment routines. First, check
    // that we have one argument provided within command line and print usage
    // info if not. Filename usually matches to regex:
    //      (([^\d]+)(\d+)-(\d+)(:?.dat))
    // Where, from two digits, the first corresponds to chunk number, and second
    // corresponds to run number.
    if( argc != 3 ) {
        fprintf( stderr,
                 "Usage:\n\t$ %s <in-filename.dat> <out-filename.mdat>\n",
                 argv[0] );
        return EXIT_FAILURE;
    }

    char * fname = rindex( argv[1], '/' );
    if( !fname ) {
        fname = argv[1];
    } else {
        ++fname;
    }
    int runNo = -1,
        chunkNo = -1;
    sscanf( fname, "cdr%d-%d.dat", &chunkNo, &runNo );
    if( -1 == runNo || -1 == chunkNo ) {
        fprintf( stderr,
                 "Unable to interpret \"%s\" filename as chunk descriptor.\n"
                 "Expected: \"cdr-<chunkNo>-<runNo>.dat\".\n", fname);
        return EXIT_FAILURE;
    }
    chunkNo -= 1000;

    // Try to open file
    std::ifstream is( argv[1] );
    if( !is ) {
        fprintf( stderr, "Unable to open file %s.\n", argv[1] );
        return EXIT_FAILURE;
    }

    // Declare indexing object:
    na64ee::DDDIndex::RunDataLayout lst;

    // Fill one indexing objectect with duty number 275 making the
    // indexing procedure remember offset of each 275-th event:
    na64ee::DDDIndex::index_chunk(
                /* std::istream instance with data ... */ is,
                /* where to fill indexes ............. */ lst,
                /* duty parameter (can be 0) ......... */ 275,
                /* guessed run number ................ */ runNo,
                /* guessed chunk number .............. */ chunkNo,
                /* logging stream C-handle ........... */ nullptr_C11 );

    is.close();

    na64ee::DDDIndex idx( runNo, lst );
    //
    idx.print_to_ostream(std::cout);
    // Serialize obtained data to file:
    size_t bufLength = idx.serialized_size();
    uint8_t * bytes = new uint8_t [bufLength];
    idx.serialize( bytes, bufLength );
    
    std::ofstream os( argv[2], std::ofstream::out | std::ofstream::trunc );
    if( !os ) {
        fprintf( stderr, "Unable to open file %s for writing.\n", argv[2] );
        return EXIT_FAILURE;
    }
    os.write( (const char*) bytes, bufLength );
    os.close();
    delete [] bytes;

    return EXIT_SUCCESS;
}

