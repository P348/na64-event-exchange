/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64ee_readout.hpp"

# include <cstdlib>
# include <fstream>

# include <DaqEvent.h>
# include <DaqEventsManager.h>

int
main( int argc, char * const argv[] ) {
    if( argc != 2 ) {
        fprintf( stderr,
                 "A simple application performing dump of event numbers at "
                 "the given .dat file.\nUsage:\n\t$ %s <in-filename.dat>\n",
                 argv[0] );
        return EXIT_FAILURE;
    }
    // Open .dat file

    CS::DaqEventsManager evm;
    evm.AddDataSource( argv[1] );

    while( evm.ReadEvent() ) {
        std::cout << evm.GetEvent().GetRunNumber() << "-"
                  << evm.GetEvent().GetBurstNumber() << "-"
                  << evm.GetEvent().GetEventNumberInBurst() << " ("
                  << evm.GetEvent().GetEventNumberInRun() << " in run)"
                  << std::endl
                  ;
    }

    return EXIT_SUCCESS;
}

