# The `reader` test application

This application utilizes one of the most basic layers of the NA64EE library.
Unfortunately, this application requires the available data chunk to perform
its operations and thus can not be included in unit testing facility.

It is concerned about basic abstraction layer were metadata caching is
performed. It deals on basic bytestream level. Physically, such stream is
always presented as std::istream (usually corresponding to "chunks" in COMPASS
DAQ).

To perform correct operations one has to specify the particular run number,
spill number and event number. They're hardcoded in test, but one can safely
change them to any existing ones.

This testing app can be used as a testing facility or a reference point for
further development of NA64EE lib internals.

