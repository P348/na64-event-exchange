/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64ee_readout.hpp"

# include <cstdlib>
# include <fstream>

# include <DaqEvent.h>

void
print_usage( const char * appName, FILE * outstream ) {
    fprintf( outstream,
        "Usage:\n\t$ %s <filename> <spillno> <chunkno> [<spillNo> <evInSpillNo>]\n"
        "This app will test simple CROAL:DaqDataDecoding testing facility in "
        "conjunction with NA64 Event Exchange (NA64EE) library on given data file. "
        "Please, point out some data file with expected run number and chunk "
        "number to read. You may also point out the spill# and event-in-spill# "
        "for testing the reading.\n"
        "The source code of this app is shipped within NA64 distro, at the "
        "apps/test_read/main.cpp and accompanied with detailed comments.\n",
        appName );
}

int
main( int argc, char * const argv[] ) {
    {
        // Mandatory printf-initialization:
        const char fmt[] = fmt_EVENTID;
        NA64EE_register_event_id_print( fmt[0] );
    }

    // Ordinary C/C++ command line arguments treatment routines. First, check
    // that we have one argument provided within command line and print usage
    // info if not. Filename usually matches to regex:
    //      (([^\d]+)(\d+)-(\d+)(:?.dat))
    // Where, from two digits, the first corresponds to chunk number, and second
    // corresponds to run number.
    bool testReadout = false;
    if( !(argc == 4 || argc == 6) ) {
        print_usage( argv[0], stderr );
        return EXIT_FAILURE;
    }
    // If spill# ev-in-spill# are given, do perform test readout.
    if( 6 == argc ) {
        testReadout = true;
    }

    // Get the run/chunk numbers from command line args:
    const unsigned short runNo = atoi(argv[2]),    // 1436
                         chunkNo = atoi(argv[3]);  // 5
    std::cout << "Expected run number: " << (int) runNo << std::endl
              << "Expected chunk number: " << (int) chunkNo << std::endl
              ;
    unsigned short spillNo = 0; // 154
    unsigned int eventInSpillNo = 0; // 1234
    if( testReadout ) {
        spillNo = atoi(argv[4]);
        eventInSpillNo = atoi(argv[5]);
        std::cout << "Spill number for testing event read: " << (int) spillNo << std::endl
                  << "Event number for testing read: " << eventInSpillNo << std::endl
              ;
    }

    // This variable can be initialized either to C-file handler or to NULL
    // indicating whether debugging output has to be printed.
    FILE * dbgStream = nullptr_C11; 
    //dbgStream = stdout; //, if output need.

    // Try to open file
    std::ifstream is( argv[1] );
    if( !is ) {
        fprintf( stderr, "Unable to open file %s.\n", argv[1] );
        return EXIT_FAILURE;
    }

    // Declare indexing object(s):
    na64ee::DDDIndex::RunDataLayout lst1, lst2;

    // Fill one indexing object. We will set its duty to 275 making the
    // indexing procedure write offset of each 275-th event:
    std::cout << "[s=" << is.tellg() << "] *** Filling second object with "
                 "duty=275..." << std::endl;
    na64ee::DDDIndex::index_chunk(
                /* std::istream instance with data ... */ is,
                /* where to fill indexes ............. */ lst1,
                /* duty parameter (can be 0) ......... */ 275,
                /* guessed run number ................ */ runNo,
                /* guessed chunk number .............. */ chunkNo,
                /* logging stream C-handle ........... */ dbgStream );

    // To continue operations with opened file one need re-set sentry to
    // its begin and clear the EOF flag:
    std::cout << "[s=" << is.tellg() << "] *** Reset and filling second "
              "object with duty=115..." << std::endl;
    is.clear();  // clear EOF flag
    is.seekg( 0, is.beg );
    na64ee::DDDIndex::index_chunk( is, lst2, 115, runNo, chunkNo, dbgStream );

    // Now we have two metadata objects indexing events in same file with
    // different duty parameter. It is possible to merge one into another
    // in scope of indexing object facility:
    na64ee::DDDIndex idx1( runNo, lst1 );
    std::cout << "Index before merge:" << std::endl;
    idx1.print_to_ostream(std::cout);
    idx1.to_JSON_str(std::cout/*, true*/);  // XXX
    std::cout << "[s=" << is.tellg() << "] *** Merging them..." << std::endl;
    size_t nMerged = idx1.merge( runNo, lst2 );
    std::cout << "Index after merge:" << std::endl;
    idx1.print_to_ostream(std::cout);
    printf( "Number of merged entries: %zu.\n", nMerged );

    // Serialize / deserealize index object test:
    size_t bufLength = idx1.serialized_size();
    uint8_t * bytes = new uint8_t [bufLength];
    size_t nBytesUsed = idx1.serialize( bytes, bufLength );
    if( nBytesUsed != bufLength ) {
        fprintf( stderr, "Mismatch of serialization buffer length: "
                "%zu (estimated) != %zu (used).\n", bufLength, nBytesUsed );
    } else {
        std::cout << "*** Serialized size for this index = " << nBytesUsed
                  << " bytes." << std::endl;
    }
    na64ee::DDDIndex & idx2 = *na64ee::DDDIndex::deserialize( bytes );
    delete [] bytes;

    if( testReadout ) {
        // Now we will try to use obtained indexing object to obtain certain event
        // from file.
        // - First, we need to assemble event ID bject with
        //   spill #: 1737, event-in-spill #: 2543:
        na64ee::EventNumericID eid
                = na64ee::assemble_event_id( runNo, spillNo, eventInSpillNo );
        // - Second, we have to get the event offset where it is stored in
        //   stream:
        na64ee::DDDIndex::FilePostion_t evPos
                              = idx2.get_event_stream_pos( is, eid, dbgStream );
        // - Now we can set the position and read event from stream:
        is.seekg( evPos, is.beg );
        // DDD provides constructor that operates directly with stream:
        CS::DaqEvent daqEvent( is );

        // Here we just making primitive checks that event is really matches
        // the specified conditions:
        if( daqEvent.GetRunNumber() != runNo
         || daqEvent.GetBurstNumber() != spillNo
         || daqEvent.GetEventNumberInBurst() != eventInSpillNo ) {
            fprintf( stderr, "Wrong event had been read.\n" );
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

