# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# This script is part of NA64EE (event exchange for NA64 experiment) library.
#
# This script performs lookup for DDD library. Since the CORAL's DDD itself
# does not provide correct cmake config-module we implemented look-up scritpt
# here. The NA64EE lib in NA64 software infrastructure is the one of the
# lowest dependencies. Please, note that DDD lib also needs Expat package to
# be installed on system.
#
# It has one optional input variable: 
#   -> DDD_LIB_PATH  --- a directory containing static library named
#                        DaqDataDecoding, libDaqDataDecoding.a or
#                        DaqDataDecoding.
# Defined variables are:
#   <- DAQDECODING_LIB_STATIC --- will be set to correct path to the library upon
#                                 successful look-up.
#   <- DAQDECODING_LIB_SHARED --- will be set to correct path to the library upon
#                                 successful look-up.
#   <- SUPPORT_DDD_FORMAT --- will be set to 1 upon successful look-up.
#   <- DAQDECODING_INCLUDE_DIR --- will be set to include path of DDD lib upon
#                                  successful look-up.

#
# Find DaqDataDecoding library
# This lib usually can be found inside of CORAL distribution.
get_filename_component( ABS_DDD_LIB_PATH
                        ${CMAKE_CURRENT_BINARY_DIR}/${DDD_LIB_PATH}
                        ABSOLUTE )

find_library( DAQDECODING_LIB_SHARED
    NAMES libDDD.so DDD.so
    HINTS ${DDD_LIB_PATH}
          ${ABS_DDD_LIB_PATH} )

find_library( DAQDECODING_LIB_STATIC
              NAMES libDDD.so DDD.so
                    libDaqDataDecoding.a DaqDataDecoding.a DaqDataDecoding
              HINTS ${DDD_LIB_PATH}
                    ${ABS_DDD_LIB_PATH} )
find_library( MONITOR_LIB_STATIC
              NAMES libmonitor.a
              HINTS ${DDD_LIB_PATH}/../../../../date/monitoring/Linux
                    ${ABS_DDD_LIB_PATH}/../../../../date/monitoring/Linux )

# Prefer shared version when available
if( DAQDECODING_LIB_SHARED )
    set( DAQDECODING_LIB ${DAQDECODING_LIB_SHARED} )
elseif( DAQDECODING_LIB_STATIC )
    set( DAQDECODING_LIB ${DAQDECODING_LIB_STATIC} )
endif( DAQDECODING_LIB_SHARED )

#
# Now try to figure out the location of header suggesting some conventional
# relations inside given prefix:
get_filename_component( DAQDECODING_INCLUDE_DIR_HINT
    ${DAQDECODING_LIB} DIRECTORY )
find_path( DAQDECODING_INCLUDE_DIR DaqEventsManager.h
    HINTS ${DAQDECODING_INCLUDE_DIR_HINT}
    ${DAQDECODING_INCLUDE_DIR_HINT}/../include
    ${DAQDECODING_INCLUDE_DIR_HINT}/../include/DaqDataDecoding
    ${DAQDECODING_INCLUDE_DIR_HINT}/../../include/coral/DaqDataDecoding )
# Try to find the exported targets from the project since they're
# carrying useful information about descendant dependencies
find_path( DAQDECODING_CMAKE_EXPORT_DIR DDDTargets.cmake
    HINTS ${DAQDECODING_INCLUDE_DIR_HINT}
    ${DAQDECODING_INCLUDE_DIR_HINT}/../lib
    ${DAQDECODING_INCLUDE_DIR_HINT}/../lib/DaqDataDecoding
    ${DAQDECODING_INCLUDE_DIR_HINT}/../lib/cmake/coral
    ${DAQDECODING_INCLUDE_DIR_HINT}/../lib/cmake/DaqDataDecoding
    ${DAQDECODING_INCLUDE_DIR_HINT}/../../lib/cmake/coral/
    ${DAQDECODING_INCLUDE_DIR_HINT}/../../lib/cmake/coral/DaqDataDecoding )

if( DAQDECODING_LIB )
    message( STATUS "Found DaqDataDecoding library at ${DAQDECODING_LIB} with header at ${DAQDECODING_INCLUDE_DIR}" )
    set( SUPPORT_DDD_FORMAT 1 )
    # Expat (required by DDD)
    find_package( EXPAT REQUIRED )
else( DAQDECODING_LIB )
    message( WARNING "DaqDataDecoding library not found. You can use "
             "DDD_LIB_PATH (preferred) or CMAKE_LIBRARY_PATH variable to "
             "manually specify the search path. Please, provide path to DDD "
             "dir with one of these options provided to CMake in "
             "-D<optname>=<path> syntax.")
endif( DAQDECODING_LIB )

if( DAQDECODING_CMAKE_EXPORT_DIR )
    message( STATUS "DaqDataDecoding CMake exported targets found at ${DAQDECODING_CMAKE_EXPORT_DIR}." )
    include( ${DAQDECODING_CMAKE_EXPORT_DIR}/DDDTargets.cmake )
    include( ${DAQDECODING_CMAKE_EXPORT_DIR}/DDDConfigVersion.cmake )
endif( DAQDECODING_CMAKE_EXPORT_DIR )

