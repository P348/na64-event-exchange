# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# This file performs look-up for certain libraries which are needed by NA64EE
# library.

#
# Find CURL (optional)
# This library provides support for HTTP event fetching. It is quite common
# lib and can be found at most of the modern Linux distributives.
find_package( CURL )
if( NOT CURL_FOUND )
    message( WARNING "CURL library was not found in your system. Online event "
            "fetching routines will not be provided by NA64EE lib." )
endif( NOT CURL_FOUND )

#
# Find Boost for unit tests (optional)
#find_package( Boost ${Boost_FORCE_VERSION} QUIET
#              COMPONENTS unit_test_framework )
#if( Boost_FOUND AND Boost_UNIT_TEST_FRAMEWORK_FOUND )
#    set( UT_ENABLED TRUE )
#endif()

