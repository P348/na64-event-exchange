/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64_event_id.hpp"

namespace na64ee {

std::ostream & operator<<( std::ostream & os,
                           const UEventID & eid ) {
    const char s[] = fmt_EVENTID;
    register_event_id_print( s[0] );
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic push
    # pragma GCC diagnostic ignored "-Wformat"
    # pragma GCC diagnostic ignored "-Wformat-extra-args"
    # endif
    char bf[64];
    snprintf( bf, sizeof(bf), "%" fmt_EVENTID, eid );
    os << bf;
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic pop
    # endif
    return os;
}

# define assign_function_alias( name ) \
    decltype( NA64EE_ ## name ) & name = NA64EE_ ## name;
for_all_NA64EE_base_eid_routines( assign_function_alias )
# undef assign_function_alias

}  // namespace na64

# if 0
na64::EventNumericID
NA64_convert_from_chunkless_event_id(
        const na64::EventNumericID chunklessId,
        const sV::Metadata metadata ) {
    _TODO_  // TODO
}

/** Recommended-to-chunkless IDs conversion routine.
 * Requires chunk metadata. */
na64::EventNumericID
NA64_convert_to_chunkless_event_id(
        const na64::EventNumericID chunklessId,
        const sV::Metadata metadata ) {
    _TODO_  // TODO
}
# endif

//
// Detector encoding tests
/////////////////////////
# ifdef UT_ENABLED

# define BOOST_TEST_MODULE Event identifier encoding
# include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( Event_ID_Encoding_suite )

BOOST_AUTO_TEST_CASE( EventIDBitfieldCoverage ) {
    const char s[] = fmt_EVENTID;
    na64ee::register_event_id_print( s[0] );
    na64ee::print_eventID_typeinfo( stdout );
    BOOST_REQUIRE( 0 == na64ee::test_eventID_typedecls( stderr ) );
}

BOOST_AUTO_TEST_CASE( EventSerializationZeroed ) {
    /*
     * Test serialize/deserialize zero values.
     */
    char strbf[2*sizeof(NA64_UEventID) + 1];
    NA64_UEventID eid;
    bzero( &eid, sizeof(eid) );

    eid.chunklessLayout.runNo = 0;
    eid.chunklessLayout.spillNo = 0;
    eid.chunklessLayout.eventNo = 0;

    eid2hex_str( eid, strbf );

    NA64_UEventID decoded;
    BOOST_REQUIRE( 0 == hex_str2eid( strbf, &decoded ) );
    BOOST_REQUIRE( 0 == decoded.chunklessLayout.runNo );
    BOOST_REQUIRE( 0 == decoded.chunklessLayout.spillNo );
    BOOST_REQUIRE( 0 == decoded.chunklessLayout.eventNo );
}

BOOST_AUTO_TEST_CASE( EventSerializationMax ) {
    /*
     * Test serialize/deserialize maximum values.
     */
    char strbf[2*sizeof(NA64_UEventID) + 1];
    NA64_UEventID eid;
    bzero( &eid, sizeof(eid) );

    eid.chunklessLayout.runNo = RUN_NO_MAX;
    eid.chunklessLayout.spillNo = SPILL_IN_RUN_NO_MAX;
    eid.chunklessLayout.eventNo = EVENTINSPILL_NO_MAX;

    eid2hex_str( eid, strbf );

    NA64_UEventID decoded;
    BOOST_REQUIRE( 0 == hex_str2eid( strbf, &decoded ) );
    BOOST_REQUIRE( RUN_NO_MAX == decoded.chunklessLayout.runNo );
    BOOST_REQUIRE( SPILL_IN_RUN_NO_MAX == decoded.chunklessLayout.spillNo );
    BOOST_REQUIRE( EVENTINSPILL_NO_MAX == decoded.chunklessLayout.eventNo );
}

BOOST_AUTO_TEST_CASE( EventSerializationRandom ) {
    /* 
     * Test serialize/deserialize for arbitrary numbers.
     */
    char strbf[2*sizeof(NA64_UEventID) + 1];
    NA64_UEventID eid;
    bzero( &eid, sizeof(eid) );

    for( unsigned short i = 0; i < 5e3; ++i ) {
        eid.chunklessLayout.runNo = (rand()*RUN_NO_MAX)/RAND_MAX;
        eid.chunklessLayout.spillNo = (rand()*SPILL_IN_RUN_NO_MAX)/RAND_MAX;
        eid.chunklessLayout.eventNo = (rand()*EVENTINSPILL_NO_MAX)/RAND_MAX;

        eid2hex_str( eid, strbf );

        NA64_UEventID decoded;
        BOOST_REQUIRE( 0 == hex_str2eid( strbf, &decoded ) );
        BOOST_REQUIRE( eid.chunklessLayout.runNo
                    == decoded.chunklessLayout.runNo );
        BOOST_REQUIRE( eid.chunklessLayout.spillNo
                    == decoded.chunklessLayout.spillNo );
        BOOST_REQUIRE( eid.chunklessLayout.eventNo
                    == decoded.chunklessLayout.eventNo );
    }
}

BOOST_AUTO_TEST_SUITE_END()
# endif  // UT_ENABLED
