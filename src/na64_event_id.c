/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64_event_id.h"
# include "na64ee_errorHandle.h"

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <printf.h>

# if __STDC_VERSION__ >= 201112L
/* Requires support of C-1x or higher: */
_Static_assert( sizeof(NA64_EventID) == sizeof(NA64_StoredEventID),
        "EventID type size mismatch #1." );
_Static_assert( sizeof(NA64_UEventID) == sizeof(NA64_StoredEventID),
        "EventID type size mismatch #2." );
_Static_assert( sizeof(NA64_UEventID) == sizeof(NA64_EventID),
        "EventID type size mismatch #3." );
_Static_assert( sizeof(NA64_UEventID) == sizeof(NA64_EventNumericID),
        "EventID type size mismatch #4." );
# else
# warning "Typelength static checks omitted since <C-1x std is used for C."
# endif

void
NA64EE_print_eventID_typeinfo( FILE * fh ) {
    fprintf( fh, "-- Common event ID types info:\n");
    fprintf( fh, "         sizeof(StoredEventID) = %zu\n",
             sizeof(NA64_StoredEventID) );
    fprintf( fh, "sizeof(AbsEventID) = %zu\n",
             sizeof(NA64_EventID) );
    fprintf( fh, "        sizeof(UEventID) = %zu\n",
             sizeof(NA64_UEventID) );
    fprintf( fh, "-- Event ID with chunk info type:\n" );
    fprintf( fh, "              RUN_NO_MAX = %lu (#%lx)\n",
             RUN_NO_MAX, RUN_NO_MAX );
    fprintf( fh, "            CHUNK_NO_MAX = %lu (#%lx)\n",
             CHUNK_NO_MAX, CHUNK_NO_MAX );
    fprintf( fh, "   SPILL_IN_CHUNK_NO_MAX = %lu (#%lx)\n",
             SPILL_IN_CHUNK_NO_MAX, SPILL_IN_CHUNK_NO_MAX );
    fprintf( fh, "     EVENTINSPILL_NO_MAX = %lu (#%lx)\n",
             EVENTINSPILL_NO_MAX, EVENTINSPILL_NO_MAX );
    fprintf( fh, "-- Event ID without chunk info type:\n" );
    fprintf( fh, "     SPILL_IN_RUN_NO_MAX = %lu (#%lx)\n",
             SPILL_IN_RUN_NO_MAX, SPILL_IN_RUN_NO_MAX );
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic push
    # pragma GCC diagnostic ignored "-Wformat"
    # pragma GCC diagnostic ignored "-Wformat-extra-args"
    # endif
    {
        NA64_EventNumericID eid = NA64EE_assemble_stored_event_id(
                    RUN_NO_MAX,
                    CHUNK_NO_MAX,
                    SPILL_IN_CHUNK_NO_MAX,
                    EVENTINSPILL_NO_MAX );
        fprintf( fh, "Recommended event format: %"
                 fmt_EVENTID
                 ".\n", eid );
    }
    {
        NA64_EventNumericID eid = NA64EE_assemble_event_id(
                    RUN_NO_MAX,
                    SPILL_IN_RUN_NO_MAX,
                    EVENTINSPILL_NO_MAX );
        fprintf( fh, "  AbsEventID event format: %"
                 fmt_EVENTID
                ".\n", eid );
    }
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic pop
    # endif
}

# if 1
int
NA64EE_test_eventID_typedecls( FILE * ersh ) {
    /* Test recommended event ID */
    {
        NA64_StoredEventID eid;
        {   /* Run number */
            bzero( &eid, sizeof(eid) ); 
            eid.runNo = RUN_NO_MAX;
            if( RUN_NO_MAX != eid.runNo ) {
                fprintf( ersh, "Run no wrong max.\n" );
                return -1;
            }
            if( eid.hasChunkNo ) {
                fprintf( ersh, "Run no exceeds its boundaries.\n" );
                return -1;
            }
        }
        {   /* Chunk number */
            bzero( &eid, sizeof(eid) ); 
            eid.chunkNo = CHUNK_NO_MAX;
            if( eid.hasChunkNo ) {
                fprintf( ersh, "Chunk no max set below its bounds.\n" );
                return -1;
            }
            if( CHUNK_NO_MAX != eid.chunkNo ) {
                fprintf( ersh, "Chunk no wrong max.\n" );
                return -1;
            }
            if( eid.spillNo ) {
                fprintf( ersh, "Chunk no max set aboive its bounds.\n" );
                return -1;
            }
        }
        {   /* Spill number */
            bzero( &eid, sizeof(eid) ); 
            eid.spillNo = SPILL_IN_CHUNK_NO_MAX;
            if( eid.chunkNo ) {
                fprintf( ersh, "Spill no max set below its bounds.\n" );
                return -1;
            }
            if( SPILL_IN_CHUNK_NO_MAX != eid.spillNo ) {
                fprintf( ersh, "Spill no wrong max.\n" );
                return -1;
            }
            if( eid.eventNo ) {
                fprintf( ersh, "Spill no max set above its bounds.\n" );
                return -1;
            }
        }
        {   /* Event number */
            bzero( &eid, sizeof(eid) ); 
            eid.eventNo = EVENTINSPILL_NO_MAX;
            if( eid.spillNo ) {
                fprintf( ersh, "Event no exceeds its bounds.\n" );
                return -1;
            }
            if( EVENTINSPILL_NO_MAX != eid.eventNo ) {
                fprintf( ersh, "Event no wrong max.\n" );
                return -1;
            }
        }
    }
    /* Test chunkless id */
    {
        NA64_EventID eid;
        {   /* Run number */
            bzero( &eid, sizeof(eid) ); 
            eid.runNo = RUN_NO_MAX;
            if( RUN_NO_MAX != eid.runNo ) {
                fprintf( ersh, "[chunkless event ID] "
                " Run no wrong max.\n" );
                return -1;
            }
            if( eid.hasChunkNo ) {
                fprintf( ersh, "[chunkless event ID] "
                " Run no exceeds its boundaries.\n" );
                return -1;
            }
        }
        {   /* Chunk number */
            bzero( &eid, sizeof(eid) ); 
            eid.spillNo = SPILL_IN_RUN_NO_MAX;
            if( eid.hasChunkNo ) {
                fprintf( ersh, "[chunkless event ID] "
                " Spill no max set below its bounds.\n" );
                return -1;
            }
            if( SPILL_IN_RUN_NO_MAX != eid.spillNo ) {
                fprintf( ersh, "[chunkless event ID] "
                " Chunk no wrong max.\n" );
                return -1;
            }
            if( eid.eventNo ) {
                fprintf( ersh, "[chunkless event ID] "
                " Chunk no max set aboive its bounds.\n" );
                return -1;
            }
        }
        {   /* Event number */
            bzero( &eid, sizeof(eid) ); 
            eid.eventNo = EVENTINSPILL_NO_MAX;
            if( eid.spillNo ) {
                fprintf( ersh, "[chunkless event ID] "
                " Event no exceeds its bounds.\n" );
                return -1;
            }
            if( EVENTINSPILL_NO_MAX != eid.eventNo ) {
                fprintf( ersh, "[chunkless event ID] "
                "Event no wrong max.\n" );
                return -1;
            }
        }
    }
    return 0;
}
# endif

NA64_EventNumericID
NA64EE_assemble_stored_event_id(
        unsigned short  runNo,
        unsigned char   chunkNo,
        unsigned short  spillNo,
        unsigned int    eventInSpillNo ) {
    NA64_UEventID eid;
    bzero( &eid, sizeof(eid) );

    if( runNo > RUN_NO_MAX ) {
        na64ee_C_error( na64ee_overflow,
                     "Run number is grater than max for its type: %lu > %lu.",
                     (unsigned long) runNo, (unsigned long) RUN_NO_MAX );
    }
    eid.layout.runNo = runNo;

    eid.layout.hasChunkNo = 0x1;

    if( chunkNo > CHUNK_NO_MAX ) {
        na64ee_C_error( na64ee_overflow,
                     "Run number is grater than max for its type: %lu > %lu.",
                     (unsigned long) chunkNo, (unsigned long) CHUNK_NO_MAX );
    }
    eid.layout.chunkNo = chunkNo;

    if( spillNo > SPILL_IN_CHUNK_NO_MAX ) {
        na64ee_C_error( na64ee_overflow,
                     "Spill number is grater than max for its type: %lu > %lu.",
                     (unsigned long) spillNo,
                     (unsigned long) SPILL_IN_CHUNK_NO_MAX );
    }
    eid.layout.spillNo = spillNo;

    if( eventInSpillNo > EVENTINSPILL_NO_MAX ) {
        na64ee_C_error( na64ee_overflow,
                     "Event number is grater than max for its type: %lu > %lu.",
                     (unsigned long) eventInSpillNo,
                     (unsigned long) EVENTINSPILL_NO_MAX );
    }
    eid.layout.eventNo = eventInSpillNo;

    return eid.numeric;
}

NA64_EventNumericID
NA64EE_assemble_event_id(
        unsigned short  runNo,
        unsigned short  spillNo,
        unsigned int    eventInSpillNo ) {
    NA64_UEventID eid;
    bzero( &eid, sizeof(eid) );

    if( runNo > RUN_NO_MAX ) {
        na64ee_C_error( na64ee_overflow,
                     "Run number is grater than max for its type: %lu > %lu.",
                     (unsigned long) runNo, (unsigned long) RUN_NO_MAX );
    }
    eid.chunklessLayout.runNo = runNo;

    eid.chunklessLayout.hasChunkNo = 0x0;

    if( spillNo > SPILL_IN_RUN_NO_MAX ) {
        na64ee_C_error( na64ee_overflow,
                     "Spill number is grater than max for its type: %lu > %lu.",
                     (unsigned long) spillNo,
                     (unsigned long) SPILL_IN_RUN_NO_MAX );
    }
    eid.chunklessLayout.spillNo = spillNo;

    if( eventInSpillNo > EVENTINSPILL_NO_MAX ) {
        na64ee_C_error( na64ee_overflow,
                     "Event number is grater than max for its type: %lu > %lu.",
                     (unsigned long) eventInSpillNo,
                     (unsigned long) EVENTINSPILL_NO_MAX );
    }
    eid.chunklessLayout.eventNo = eventInSpillNo;

    return eid.numeric;
}

size_t
eid2str( NA64_UEventID eid, char * buf, size_t buflen) {
    if( eid.layout.hasChunkNo ) {
        return snprintf( buf, buflen, "%hu:%hu/%hu-%u",
            (uint16_t) eid.layout.runNo,
            (uint16_t) eid.layout.chunkNo,
            (uint16_t) eid.layout.spillNo,
            (uint32_t) eid.layout.eventNo );
    } else {
        return snprintf( buf, buflen, "%hu/%hu-%u",
            (uint16_t) eid.chunklessLayout.runNo,
            (uint16_t) eid.chunklessLayout.spillNo,
            (uint32_t) eid.chunklessLayout.eventNo );
    }
}

/*
 * Customizing printf() providing event ID correct printing
 */

static int
print_eventID( FILE *stream,
               const struct printf_info * info,
               const void * const * args) {
    char *buffer;
    int len;

    const NA64_UEventID eid = *((NA64_UEventID*) *args);
    if( eid.layout.hasChunkNo ) {
        len = asprintf( &buffer, "%hu:%hu/%hu-%u",
            (uint16_t) eid.layout.runNo,
            (uint16_t) eid.layout.chunkNo,
            (uint16_t) eid.layout.spillNo,
            (uint32_t) eid.layout.eventNo );
    } else {
        len = asprintf( &buffer, "%hu/%hu-%u",
            (uint16_t) eid.chunklessLayout.runNo,
            (uint16_t) eid.chunklessLayout.spillNo,
            (uint32_t) eid.chunklessLayout.eventNo );
    }
    if( len == -1 ) {
        return -1;
    }

    /* Pad to the minimum field width and print to the stream. */
    len = fprintf( stream, "%*s",
                   (info->left ? -info->width : info->width),
                   buffer);

    /* Clean up and return. */
    free( buffer );
    return len;
}

static int pa_64evid = 0;

static void
_static_pa_na64evid( void * mem, va_list * ap ) {
    union NA64_UEventID d = va_arg( *ap, NA64_UEventID );
    memcpy( mem, &d, sizeof(NA64_UEventID) );
    /*printf( "XXX: %lu\n", ((union NA64_UEventID*) mem)->layout.eventNo );*/
}

static int
print_eventID_arginfo(const struct printf_info * info,
                      size_t n,
                      int * argtype,
                      int * size) {
    if( n > 0 ) {
        # if 1
        argtype[0] = PA_POINTER;
        /* ^^^ TODO : we use here PA_POqINTER because for x86_64 it seems to
         * provide a valid copy-to function for printf(). We have to customize
         * the type, however with pa_64evid obtained by register_printf_type(),
         * but this way seems to violate something causing output values be
         * malformed. */
        # else
        argtype[0] = pa_64evid;
        # endif
        size[0] = sizeof( NA64_UEventID );
    }
    return 1;
}

void
NA64EE_register_event_id_print( const char sym ) {
    if( !pa_64evid ) {
        pa_64evid = register_printf_type( _static_pa_na64evid );
        register_printf_specifier( sym, print_eventID, print_eventID_arginfo );
    }
}

static const char _static_hexLetters[] = "0123456789abcdef";
void
eid2hex_str( NA64_UEventID eid, char * buffer ) {
    const unsigned char * cBgn = (unsigned char *) &eid,
                        * cEnd = cBgn + sizeof(NA64_EventID)
                        ;
    char * cBf = buffer;
    const char * bs = _static_hexLetters;
    for( unsigned char * c = (unsigned char *) &eid;
         c < cEnd; ++c, cBf+=2 ) {
        *(cBf  ) = bs[( *c >> 4) & 0xf];
        *(cBf+1) = bs[( *c     ) & 0xf];
    }
    *(cBf) = '\0';
}

static unsigned char
char_to_halfbyte( unsigned char c ) {
    if( c >= '0' && c <= '9' ) {
        return (c - '0');
    } else if( c >= 'a' && c < 'g' ) {
        return (c - 'a') + 10;
    }
    return 0xff;
}

int
hex_str2eid(    const char * buffer,
                NA64_UEventID * resultPtr ) {
    const char * cBf;
    unsigned char * c = (unsigned char *) resultPtr
                  ;
    bzero( resultPtr, sizeof(NA64_UEventID) );
    for( cBf = buffer; '\0' != *cBf; cBf +=2, ++c ) {
        unsigned char c1 = char_to_halfbyte( *(cBf    ) ),
                      c2 = char_to_halfbyte( *(cBf + 1) )
             ;
        if(  0xff == c1
          || 0xff == c2 ) {
            /* Nullate output object and return. */
            bzero( resultPtr, sizeof(NA64_UEventID) );
            return -1;
        }
        *c = (c1 << 4)
           | (c2     )
           ;
    }
    return 0;
}

