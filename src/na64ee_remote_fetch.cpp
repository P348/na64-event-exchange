/*
 * Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64ee_remote_fetch.hpp"

# include <list>
# include <vector>
# include <curl/curl.h>
# include <cstring>

namespace na64ee {

struct MemoryStruct {
    /// Will be filled and freed with internal routines.
    std::list< std::vector<uint8_t> > chunks;
};

static size_t
_static_url_fetch_cllb(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size*nmemb;
    reinterpret_cast<MemoryStruct *>(userp) 
                ->chunks.emplace_back( (uint8_t *) contents,
                                      ((uint8_t *) contents) + realsize );
    return realsize;
}

void
fetch_remote_data( const char * url, std::vector<uint8_t> & data ) {
    CURL *curlHandle;
    CURLcode res;
    struct MemoryStruct m;
    curl_global_init(CURL_GLOBAL_ALL);
    // init the curl session
    curlHandle = curl_easy_init();
    // specify URL to get
    curl_easy_setopt(curlHandle, CURLOPT_URL, url);
    // send all data to this function
    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION,
                                                    _static_url_fetch_cllb);
    // we pass our 'chunk' struct to the callback function
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, (void *)&m);
    // some servers don't like requests that are made without a user-agent
    // field, so we provide one
    curl_easy_setopt(curlHandle, CURLOPT_USERAGENT, "na64ee-libcurl/" STRINGIFY_MACRO_ARG(NA64EE_VERSION) );
    // get it!
    res = curl_easy_perform(curlHandle);
    // check for errors
    if(res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                        curl_easy_strerror(res));
    } else {
        long httpRespCode = 0;
        char * ct;
        curl_easy_getinfo( curlHandle, CURLINFO_RESPONSE_CODE, &httpRespCode);
        curl_easy_getinfo( curlHandle, CURLINFO_CONTENT_TYPE, &ct);

        data.clear();
        size_t fullSize = 0;
        //for( const auto & v : m.chunks )
        for( auto it = m.chunks.begin(); m.chunks.end() != it; ++it ) {
            fullSize += it->size();
        }
        data.reserve( fullSize );
        //for( const auto & v : m.chunks )
        for( auto it = m.chunks.begin(); m.chunks.end() != it; ++it ) {
            data.insert( data.end(), it->cbegin(), it->cend() );
        }

        if( 200 != httpRespCode ) {
            std::cerr << "HTTP response code: " << httpRespCode << std::endl
                      << " - content-type: " << ct << std::endl
                  ;
            if( !strncmp(ct, "text", 4) ) {
                std::cerr << " --- " << ct << " BGN ---" << std::endl
                          << ((char *) data.data()) << std::endl
                          << " --- " << ct << " END ---" << std::endl
                          ;
            }
        }
    }
    // cleanup curl stuff
    curl_easy_cleanup(curlHandle);
}

}  // namespace na64ee

